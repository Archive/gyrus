/*
  gyrus-main-app.c

  GYRUS -- GNOME Cyrus Administrator Main Application Class.

  Copyright (C) 2003-2004 Alejandro Valdes J.
  Copyright (C) 2003-2004 Jorge Bustos B.
  Copyright (C) 2003-2004 Claudio Saavedra V.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, see <http://www.gnu.org/licenses/>.
  
*/

#include <config.h>

#include <gtk/gtk.h>
#include <stdlib.h>

#include "glib/gi18n.h"
#include "gyrus-dialog-mailbox-new.h"
#include "gyrus-dialog-find-mailbox.h"
#include "gyrus-main-app.h"
#include "gyrus-session.h"
#include "gyrus-admin-mailbox.h"
#include "gyrus-report.h"

#define GYRUS_MAIN_APP_GET_PRIVATE(object)         \
	G_TYPE_INSTANCE_GET_PRIVATE (object, GYRUS_TYPE_MAIN_APP, GyrusMainAppPrivate);

G_DEFINE_TYPE (GyrusMainApp, gyrus_main_app, GTK_TYPE_WINDOW)

/*** Gyrus Main Application Private Data ***/
struct _GyrusMainAppPrivate {
        GtkWidget *notebook;
        GtkWidget *statusbar;
        GtkWidget *progress_bar;
        GtkUIManager *ui_manager;
        GtkActionGroup *actions_window;
        GtkActionGroup *actions_server;
        GtkActionGroup *actions_mailbox;
        GtkActionGroup *actions_acl_item;
        guint tip_message_cid;
};

static void gyrus_main_app_about (void);

static void
connect_proxy_cb (GtkUIManager *manager,
                  GtkAction *action,
                  GtkWidget *proxy,
                  GyrusMainApp *app);

static void
disconnect_proxy_cb (GtkUIManager *manager,
                     GtkAction *action,
                     GtkWidget *proxy,
                     GyrusMainApp *app);

/*** Class Callbacks ***/

static void
gyrus_main_app_on_file_open_session (GtkWidget *widget G_GNUC_UNUSED,
				     gpointer user_data)
{
	gyrus_session_show_sessions (NULL, user_data);
}

static void
gyrus_main_app_on_file_exit (GtkWidget *widget G_GNUC_UNUSED, 
			     gpointer user_data G_GNUC_UNUSED)
{
	gtk_main_quit ();
}

static void
gyrus_main_app_on_edit_find (GtkWidget *widget G_GNUC_UNUSED, 
			     gpointer user_data)
{
	GyrusMainApp *app  = GYRUS_MAIN_APP (user_data);

	GtkWidget *dialog =
		gyrus_dialog_find_mailbox_new ();
	
	gtk_window_set_transient_for (GTK_WINDOW (dialog),
				      GTK_WINDOW (user_data));
	gyrus_dialog_find_mailbox_set_admin (GYRUS_DIALOG_FIND_MAILBOX (dialog),
					     gyrus_main_app_get_current_admin (app));
	
	gtk_widget_show (dialog);
}

static void
gyrus_main_app_on_edit_mailbox_add (GtkWidget *widget G_GNUC_UNUSED, 
				    gpointer user_data)
{
	GtkWidget *dialog =
		gyrus_dialog_mailbox_new_new ();
	gtk_widget_show (dialog);
	gtk_window_set_transient_for (GTK_WINDOW (dialog),
				      GTK_WINDOW (user_data));
	
}

static void
gyrus_admin_delete_dialog_on_response (GtkDialog *dialog,
				       gint result,
				       gpointer user_data)
{
	GyrusAdmin * admin = GYRUS_ADMIN (user_data);

	if (result == GTK_RESPONSE_YES) {
		gchar *mailbox = gyrus_admin_get_selected_mailbox (admin);
		gyrus_admin_mailbox_delete_all (admin, mailbox);
		gyrus_admin_refresh_users_list (admin);
		gyrus_admin_mailbox_clear_info (admin);
		g_free (mailbox);
	}

	gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void
gyrus_main_app_on_edit_mailbox_remove (GtkWidget *widget G_GNUC_UNUSED,
				       gpointer user_data)
{
	GyrusMainApp *app  = GYRUS_MAIN_APP (user_data);
	GyrusAdmin * admin = gyrus_main_app_get_current_admin (app);

	gchar *mailbox = gyrus_admin_get_selected_mailbox (admin);

	GtkWidget *dialog = gtk_message_dialog_new
		(GTK_WINDOW (app), GTK_DIALOG_DESTROY_WITH_PARENT,
		 GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO,
		 _("Really delete mailbox '%s' and all of its submailboxes?"),
		 mailbox);
	
	g_signal_connect (G_OBJECT (dialog), "response",
			  G_CALLBACK (gyrus_admin_delete_dialog_on_response),
			  admin);
	
	gtk_dialog_run (GTK_DIALOG (dialog));
	
	g_free (mailbox);
}

static void
gyrus_main_app_on_acl_entry_delete (GtkWidget *widget G_GNUC_UNUSED,
				    gpointer user_data)
{
	GyrusMainApp *app  = GYRUS_MAIN_APP (user_data);
	GyrusAdmin * admin = gyrus_main_app_get_current_admin (app);

	gyrus_admin_delete_selected_acl_entry (admin);
}

static void
gyrus_main_app_on_acl_entry_rename (GtkWidget *widget G_GNUC_UNUSED,
				    gpointer user_data)
{
	GyrusMainApp *app  = GYRUS_MAIN_APP (user_data);
	GyrusAdmin * admin = gyrus_main_app_get_current_admin (app);

	gyrus_admin_start_editing_selected_acl (admin);
}

static void
gyrus_main_app_on_acl_entry_new (GtkWidget *widget G_GNUC_UNUSED,
				 gpointer user_data)
{
	GyrusMainApp *app  = GYRUS_MAIN_APP (user_data);
	GyrusAdmin * admin = gyrus_main_app_get_current_admin (app);

	gyrus_admin_add_acl_entry (admin);
}

static void
gyrus_main_app_on_view_refresh (GtkWidget *widget G_GNUC_UNUSED,
				       gpointer user_data)
{
	GyrusMainApp *app  = GYRUS_MAIN_APP (user_data);
	GyrusAdmin * admin = gyrus_main_app_get_current_admin (app);
	gyrus_admin_refresh_users_list (admin);
}

static void
gyrus_main_app_on_view_report (GtkWidget *widget G_GNUC_UNUSED,
				       gpointer user_data)
{
	GyrusMainApp *app  = GYRUS_MAIN_APP (user_data);
	GyrusAdmin *admin = gyrus_main_app_get_current_admin (app);

	gyrus_report_show_report(admin);
}

static void
gyrus_main_app_on_help_about (GtkWidget *widget G_GNUC_UNUSED,
				       gpointer user_data)
{
	gyrus_main_app_about();
}

static void
gyrus_main_app_make_sensitivity_consistent (GyrusMainApp *app)
{
	gint n_page;
	GyrusAdmin *admin;

	n_page = gtk_notebook_get_current_page
		(GTK_NOTEBOOK (app->priv->notebook));

	/* If there are no pages left in Notebook */
	if (n_page < 0) {
		gtk_action_group_set_sensitive (app->priv->actions_server, FALSE);
		gtk_action_group_set_sensitive (app->priv->actions_mailbox, FALSE);
		gtk_action_group_set_sensitive (app->priv->actions_acl_item, FALSE);
		return;
	}
	
	admin = GYRUS_ADMIN
		(gtk_notebook_get_nth_page (GTK_NOTEBOOK (app->priv->notebook),
					    n_page));
	
	if (gyrus_admin_is_connected (admin)) {
		gtk_action_group_set_sensitive (app->priv->actions_server, TRUE);
		gtk_action_group_set_sensitive (app->priv->actions_acl_item, 
						gyrus_admin_acl_has_selection (admin));
	}
	else
		gtk_action_group_set_sensitive (app->priv->actions_server, FALSE);
}

static void
gyrus_main_app_on_button_close_page_clicked (GtkButton *button, gpointer data)
{
	gint page_number;
	GtkWidget    *admin   = GTK_WIDGET (data);
	gpointer     app_data = g_object_get_data (G_OBJECT (button),
						   "parent-app");
	GyrusMainApp *app     = GYRUS_MAIN_APP (app_data);
	
	page_number = gtk_notebook_page_num (GTK_NOTEBOOK
					     (app->priv->notebook), admin);
	gtk_notebook_remove_page (GTK_NOTEBOOK
				  (app->priv->notebook), page_number);

	gyrus_main_app_make_sensitivity_consistent (app);
	
	if (gtk_notebook_get_n_pages (GTK_NOTEBOOK (app->priv->notebook)) == 0)
		gtk_window_set_title (GTK_WINDOW (app), _("Cyrus IMAP Administrator"));
}

static void
gyrus_main_app_on_switch_page (GtkNotebook *notebook,
			       gpointer *page,
			       guint page_num,
			       gpointer user_data)
{
	GyrusMainApp *app = GYRUS_MAIN_APP (user_data);
	GyrusAdmin *admin = GYRUS_ADMIN (gtk_notebook_get_nth_page (notebook,
								    page_num));
	
	const gchar *session_name = gyrus_admin_get_current_session_name (admin);
	gchar *title = g_strdup_printf (_("%s - Cyrus IMAP Administrator"), 
					session_name);
	gchar *mailbox;
	
	gtk_window_set_title (GTK_WINDOW (app), title);
	g_free (title);


	if (gyrus_admin_is_connected (admin))
		gtk_action_group_set_sensitive (app->priv->actions_server, TRUE);
	else
		gtk_action_group_set_sensitive (app->priv->actions_server, FALSE);

	if ((mailbox = gyrus_admin_get_selected_mailbox (admin)) != NULL) {
		gtk_action_group_set_sensitive (app->priv->actions_mailbox, 
						TRUE);
		if (gyrus_admin_has_current_acl_access (admin) == FALSE)
			/* TODO: entry_new to false */ ;
		g_free (mailbox);
	}
	else {
		gtk_action_group_set_sensitive (app->priv->actions_mailbox,
						FALSE);
	}
	gtk_action_group_set_sensitive (app->priv->actions_acl_item,
					gyrus_admin_acl_has_selection (admin));
}

/*** Private Methods ***/

static void
gyrus_main_app_class_init (GyrusMainAppClass *class)
{
	gyrus_main_app_parent_class = g_type_class_peek_parent (class);

	/* register private struct */
	g_type_class_add_private (class, sizeof(GyrusMainAppPrivate));
}

static const char *ui_description =
"<ui>\n"
"  <menubar name='MainMenu'>\n"
"    <menu action='FileMenu'>\n"
"      <menuitem action='OpenSession'/>\n"
"      <separator/>\n"
"      <menuitem action='Quit'/>\n"
"    </menu>\n"
"    <menu action='EditMenu'>\n"
"      <menuitem action='Find'/>\n"
"    </menu>\n"
"    <menu action='AclMenu'>\n"
"      <menuitem action='EntryNew'/>\n"
"      <menuitem action='EntryRename'/>\n"
"      <menuitem action='EntryDelete'/>\n"
"    </menu>\n"
"    <menu action='ViewMenu'>\n"
"      <menuitem action='Refresh'/>\n"
"      <separator/>\n"
"      <menuitem action='Report'/>\n"
"    </menu>\n"
"    <menu action='HelpMenu'>\n"
"      <menuitem action='About'/>\n"
"    </menu>\n"
"  </menubar>\n"
"  <toolbar name='MainToolbar'>\n"
"    <placeholder name='placeholder1'>\n"
"      <toolitem action='OpenSession' />\n"
"      <separator/>\n"
"      <toolitem action='AddMailbox' />\n"
"      <toolitem action='RemoveMailbox' />\n"
"      <separator/>\n"
"      <toolitem action='Find' />\n"
"      <separator/>\n"
"      <toolitem action='Refresh'/>\n"
"    </placeholder>\n"
"  </toolbar>\n"
"</ui>\n";

static const GtkActionEntry entries_window[] = {
	{ "FileMenu", NULL, N_("_File") },
	{ "EditMenu", NULL, N_("_Edit") },
	{ "AclMenu", NULL, N_("_ACL") },
	{ "ViewMenu", NULL, N_("_View") },
	{ "HelpMenu", NULL, N_("_Help") },
	{ "OpenSession", GTK_STOCK_OPEN, N_("Go to server..."), "<control>O", 
	  N_("Show the list of servers"), G_CALLBACK (gyrus_main_app_on_file_open_session) },
	{ "Quit", GTK_STOCK_QUIT, NULL, NULL, NULL,
	  G_CALLBACK (gyrus_main_app_on_file_exit) },
	{ "About", GTK_STOCK_ABOUT, NULL, NULL, NULL, 
	  G_CALLBACK (gyrus_main_app_on_help_about) }
};

static const GtkActionEntry entries_server[] = {
  { "AddMailbox", GTK_STOCK_ADD, N_("Add mailbox"), NULL,
    N_("Add a mailbox under the one selected"), G_CALLBACK (gyrus_main_app_on_edit_mailbox_add) },
  { "Find", GTK_STOCK_FIND, NULL, NULL, 
    N_("Search for a mailbox in current server"), G_CALLBACK (gyrus_main_app_on_edit_find) },
  { "Refresh", GTK_STOCK_REFRESH, NULL, "<control>R", 
    N_("Refresh the mailbox list"), G_CALLBACK (gyrus_main_app_on_view_refresh) },
  { "Report", NULL, N_("Create report..."), NULL, 
    N_("Create report of users with quota problems"), G_CALLBACK (gyrus_main_app_on_view_report) }
};

static const GtkActionEntry entries_mailbox[] = {
  { "EntryNew", NULL, N_("New entry"), "<control><shift>N", 
    N_("Create a new ACL entry in current mailbox"), G_CALLBACK (gyrus_main_app_on_acl_entry_new) },
  { "RemoveMailbox", GTK_STOCK_REMOVE, N_("Remove mailbox"), NULL,
    N_("Remove current mailbox from the server"), G_CALLBACK (gyrus_main_app_on_edit_mailbox_remove) }
};

static const GtkActionEntry entries_acl_item [] = {

  { "EntryRename", NULL, N_("Rename entry"), "<control><shift>R", 
    N_("Rename selected ACL entry"), G_CALLBACK (gyrus_main_app_on_acl_entry_rename) },
  { "EntryDelete", NULL, N_("Delete entry"), "<control><shift>D", 
    N_("Delete selected ACL entry"), G_CALLBACK (gyrus_main_app_on_acl_entry_delete) }
};

static void
gyrus_main_app_init (GyrusMainApp *app)
{

	GtkWidget *main_vbox;
	GtkWidget *menubar;
	GtkWidget *toolbar;
	GtkAccelGroup *accel_group;
	GError *error;

	/* get a private struct */
	app->priv = GYRUS_MAIN_APP_GET_PRIVATE (app);

	gtk_window_set_title (GTK_WINDOW (app),
			      _("Cyrus IMAP Administrator"));

	main_vbox = gtk_vbox_new (FALSE, 0);

	app->priv->ui_manager = gtk_ui_manager_new ();

	app->priv->actions_window = gtk_action_group_new ("Window Actions");
	gtk_action_group_set_translation_domain (app->priv->actions_window,
						 GETTEXT_PACKAGE);
	gtk_action_group_add_actions (app->priv->actions_window, entries_window, 
				      G_N_ELEMENTS (entries_window), app);
	gtk_ui_manager_insert_action_group (app->priv->ui_manager, app->priv->actions_window, 0);

	app->priv->actions_server = gtk_action_group_new ("Server Actions");
	gtk_action_group_set_translation_domain (app->priv->actions_server,
						 GETTEXT_PACKAGE);
	gtk_action_group_add_actions (app->priv->actions_server, entries_server, 
				      G_N_ELEMENTS (entries_server), app);
	gtk_ui_manager_insert_action_group (app->priv->ui_manager, app->priv->actions_server, 0);
	gtk_action_group_set_sensitive (app->priv->actions_server, FALSE);

	app->priv->actions_mailbox = gtk_action_group_new ("Mailbox Actions");
	gtk_action_group_set_translation_domain (app->priv->actions_mailbox,
						 GETTEXT_PACKAGE);
	gtk_action_group_add_actions (app->priv->actions_mailbox, entries_mailbox, 
				      G_N_ELEMENTS (entries_mailbox), app);
	gtk_ui_manager_insert_action_group (app->priv->ui_manager, app->priv->actions_mailbox, 0);
	gtk_action_group_set_sensitive (app->priv->actions_mailbox, FALSE);

	app->priv->actions_acl_item = gtk_action_group_new ("ACL Item Actions");
	gtk_action_group_set_translation_domain (app->priv->actions_acl_item,
						 GETTEXT_PACKAGE);
	gtk_action_group_add_actions (app->priv->actions_acl_item, entries_acl_item, 
				      G_N_ELEMENTS (entries_acl_item), app);
	gtk_ui_manager_insert_action_group (app->priv->ui_manager, app->priv->actions_acl_item, 0);
	gtk_action_group_set_sensitive (app->priv->actions_acl_item, FALSE);

	accel_group = gtk_ui_manager_get_accel_group (app->priv->ui_manager);
	gtk_window_add_accel_group (GTK_WINDOW (app), accel_group);

	error = NULL;
	if (!gtk_ui_manager_add_ui_from_string (app->priv->ui_manager, ui_description, -1, &error))
	{
		g_message ("building menus failed: %s", error->message);
		g_error_free (error);
		exit (EXIT_FAILURE);
	}

	g_signal_connect (app->priv->ui_manager, "connect_proxy",
			  G_CALLBACK (connect_proxy_cb), app);
	g_signal_connect (app->priv->ui_manager, "disconnect_proxy",
			  G_CALLBACK (disconnect_proxy_cb), app);

	menubar = gtk_ui_manager_get_widget (app->priv->ui_manager, "/MainMenu");
	gtk_box_pack_start (GTK_BOX (main_vbox), menubar, FALSE, FALSE, 0);

	toolbar = gtk_ui_manager_get_widget (app->priv->ui_manager, "/MainToolbar");
	gtk_box_pack_start (GTK_BOX (main_vbox), toolbar, FALSE, FALSE, 0);

	app->priv->notebook = gtk_notebook_new ();

	g_signal_connect (G_OBJECT (app->priv->notebook), "switch-page",
			  G_CALLBACK (gyrus_main_app_on_switch_page), app);
	
	gtk_box_pack_start (GTK_BOX (main_vbox), app->priv->notebook,
			    TRUE, TRUE, 0);

	app->priv->statusbar = gtk_statusbar_new ();

	app->priv->progress_bar = gtk_progress_bar_new ();

	app->priv->tip_message_cid = 
		gtk_statusbar_get_context_id (GTK_STATUSBAR (app->priv->statusbar), 
					      "tip_message");

	gtk_box_pack_end (GTK_BOX (app->priv->statusbar), app->priv->progress_bar,
			    FALSE, FALSE, 0);

	gtk_box_pack_start (GTK_BOX (main_vbox), app->priv->statusbar,
			    FALSE, FALSE, 0);

	gtk_container_add (GTK_CONTAINER (app), 
			   main_vbox);

	gtk_widget_show_all (main_vbox);
}

GyrusAdmin *
gyrus_main_app_get_current_admin (GyrusMainApp *app)
{
	GyrusAdmin *admin;
	gint n;
	n = gtk_notebook_get_current_page (GTK_NOTEBOOK (app->priv->notebook));
	admin = GYRUS_ADMIN (gtk_notebook_get_nth_page
			     (GTK_NOTEBOOK (app->priv->notebook),
			      n));

	return admin;
}

static void
gyrus_main_app_about (void)
{
	GdkPixbuf *pixbuf;
	gchar *logo_gyrus;

	const gchar *authors[] = {
		"Alejandro Vald" "\xC3\xA9" "s Jim" "\xC3\xA9"
		"nez <avaldes@utalca.cl>",
		"Jorge Bustos Bustos <jbustos@utalca.cl>",
		"Claudio Saavedra "
		"<csaavedra@igalia.com>",
		"Francisco Rojas <frojas@alumnos.utalca.cl>",
		NULL
	};

	const gchar *translators = _("translators-credits");

	logo_gyrus = g_strdup_printf ("%s%c%s", GYRUS_PIXMAPS_DIR,
	                              G_DIR_SEPARATOR, "logo_gyrus.png");
	                              
	pixbuf = gdk_pixbuf_new_from_file(logo_gyrus, NULL);
	
	g_free (logo_gyrus);
	
	gtk_show_about_dialog (NULL,
			       "name", _("GNOME Cyrus Administrator"),
			       "version", VERSION,
			       "copyright", _("(c) 2003-2005 GNOME Foundation\n"
					      "(c) 2004-2005 Claudio Saavedra"),
			       "comments", _("Administration tool for Cyrus IMAP servers."),
			       "authors", authors, 
			       "translator-credits", strcmp (translators, "translators-credits") != 0 ? translators : NULL,
			       "logo", pixbuf,
			       "website", "http://www.gnome.org/projects/gyrus",
			       NULL);

	g_object_unref(pixbuf);
}

static GtkWidget *
gyrus_main_app_create_label_from_admin (GyrusMainApp *app, GyrusAdmin *admin)
{
	GtkWidget *hbox;
	GtkWidget *image;
	GtkWidget *button;
	GtkWidget *label;

	const gchar *session_name;
	
	/* create a text label */
	session_name = gyrus_admin_get_current_session_name (admin);
	label = gtk_label_new (session_name ? session_name : "");
	
	/* create a button with a close icon */
	image = gtk_image_new_from_stock (GTK_STOCK_CLOSE, GTK_ICON_SIZE_MENU);
	button = gtk_button_new ();
	gtk_button_set_relief (GTK_BUTTON (button), GTK_RELIEF_NONE);
	gtk_widget_set_size_request (button, 20, 20);
	gtk_container_add (GTK_CONTAINER (button), image);
	g_object_set_data (G_OBJECT (button), "parent-app", app);

	/* create the container for both widgets*/
	hbox = gtk_hbox_new (FALSE, 5);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);

	gtk_widget_show_all (hbox);

	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (gyrus_main_app_on_button_close_page_clicked),
			  admin);

	return hbox;
}

/*** Public Methods ****/
GtkWidget *
gyrus_main_app_new (void)
{
	GyrusMainApp *app;
	app = g_object_new (GYRUS_TYPE_MAIN_APP, NULL);
	return GTK_WIDGET (app);
}

static void
main_app_on_admin_connected (GyrusAdmin *admin,
			     gpointer *data)
{
	GyrusMainApp *app = GYRUS_MAIN_APP (data);
	gtk_action_group_set_sensitive (app->priv->actions_server,
					TRUE);
}

static void
main_app_on_admin_disconnected (GyrusAdmin *admin,
				gpointer *data)
{
	GyrusMainApp *app = GYRUS_MAIN_APP (data);
	gtk_action_group_set_sensitive (app->priv->actions_server,
					FALSE);
	gtk_action_group_set_sensitive (app->priv->actions_mailbox,
					FALSE);
}

static void
main_app_on_acl_selection_changed (GyrusAdmin *admin,
				   gboolean selected,
				   gpointer *data)
{
	GyrusMainApp *app = GYRUS_MAIN_APP (data);
	gtk_action_group_set_sensitive (app->priv->actions_acl_item,
					selected);
}

static void
main_app_on_mailbox_selection_changed (GyrusAdmin *admin,
				       gboolean selected,
				       gpointer *data)
{
	GyrusMainApp *app = GYRUS_MAIN_APP (data);
	gtk_action_group_set_sensitive (app->priv->actions_mailbox,
					selected);
}

GtkWidget *
gyrus_main_app_append_page (GyrusMainApp *app, GyrusSession *session)
{
	GtkWidget *hbox_label;
	gint page_num;
	GtkWidget *admin;

	g_return_val_if_fail (GYRUS_IS_MAIN_APP (app), NULL);

	admin = gyrus_admin_new (session);

	g_signal_connect (G_OBJECT (admin), "connected", 
			  G_CALLBACK (main_app_on_admin_connected), 
			  app);

	g_signal_connect (G_OBJECT (admin), "disconnected", 
			  G_CALLBACK (main_app_on_admin_disconnected), 
			  app);
	
	g_signal_connect (G_OBJECT (admin), "acl-selection-changed", 
			  G_CALLBACK (main_app_on_acl_selection_changed), 
			  app);

	g_signal_connect (G_OBJECT (admin), "mailbox-selection-changed", 
			  G_CALLBACK (main_app_on_mailbox_selection_changed), 
			  app);
	
	gtk_notebook_append_page (GTK_NOTEBOOK (app->priv->notebook),
				  admin, NULL);
	
	hbox_label = gyrus_main_app_create_label_from_admin
		(app, GYRUS_ADMIN (admin));
	gtk_notebook_set_tab_label (GTK_NOTEBOOK (app->priv->notebook),
				    admin, hbox_label);

	page_num = gtk_notebook_page_num (GTK_NOTEBOOK (app->priv->notebook),
					  admin);
	gtk_notebook_set_current_page (GTK_NOTEBOOK (app->priv->notebook),
				       page_num);
	
	return admin;
}

void
gyrus_main_app_menu_set_sensitive (GyrusMainApp *app, gboolean sens)
{
	g_return_if_fail (GYRUS_IS_MAIN_APP (app));
	gtk_action_group_set_sensitive (app->priv->actions_server,
					sens);
}

/***** Above functions doesnt belong this file ******/

static void
gyrus_application_window_destroyed_callback (GtkWindow *window,
					     gpointer userdata)
{
	gtk_main_quit();
}

int
main (int argc, char *argv[])
{
	GtkWidget *app;
	gtk_init (&argc, &argv);
	bindtextdomain (GETTEXT_PACKAGE, GNOME_LOCALE_DIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	gtk_init (&argc, &argv);
	g_set_prgname ("gyrus");
	g_set_application_name (_("Cyrus IMAP Administrator"));

	app = gyrus_main_app_new ();
	gtk_window_set_default_size (GTK_WINDOW (app), 600, 400);
	g_signal_connect (GTK_OBJECT (app), "delete-event",
			  G_CALLBACK
			  (gyrus_application_window_destroyed_callback),
			  NULL);

	gtk_widget_show (GTK_WIDGET (app));
	gtk_main ();

	return 0;
}

static void
menu_item_select_cb (GtkMenuItem *proxy, GyrusMainApp *app)
{
	GtkAction *action;
	char *message;

	action = g_object_get_data (G_OBJECT (proxy), "gtk-action");

	g_return_if_fail (action != NULL);

	g_object_get (G_OBJECT (action), "tooltip", &message, NULL);

	if (message) {
		gtk_statusbar_push (GTK_STATUSBAR (app->priv->statusbar),
				    app->priv->tip_message_cid, message);
		g_free (message);
	}
}

static void
menu_item_deselect_cb (GtkMenuItem *proxy, GyrusMainApp *app)
{
	gtk_statusbar_pop (GTK_STATUSBAR (app->priv->statusbar),
			   app->priv->tip_message_cid);
}

static void
connect_proxy_cb (GtkUIManager *manager,
                  GtkAction *action,
                  GtkWidget *proxy,
                  GyrusMainApp *app)
{
	if (GTK_IS_MENU_ITEM (proxy)) {
		g_signal_connect (proxy, "select",
				  G_CALLBACK (menu_item_select_cb), app);
		g_signal_connect (proxy, "deselect",
				  G_CALLBACK (menu_item_deselect_cb), app);
	}
}

static void
disconnect_proxy_cb (GtkUIManager *manager,
                     GtkAction *action,
                     GtkWidget *proxy,
                     GyrusMainApp *app)
{
	if (GTK_IS_MENU_ITEM (proxy)) {
		g_signal_handlers_disconnect_by_func
			(proxy, G_CALLBACK (menu_item_select_cb), app);
		g_signal_handlers_disconnect_by_func
			(proxy, G_CALLBACK (menu_item_deselect_cb), app);
	}
}
