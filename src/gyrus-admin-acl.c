/*
  gyrus-admin-acl.c

  GYRUS -- GNOME Cyrus Administrator. ACL support.
  
  Copyright (C) 2004-2005 Claudio Saavedra V. <csaavedra@alumnos.utalca.cl>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, see <http://www.gnu.org/licenses/>.

*/

#include <config.h>

#include <gtk/gtk.h>
#include <string.h>
#include <ctype.h>

#include "glib/gi18n.h"
#include "gyrus-main-app.h"
#include "gyrus-admin.h"
#include "gyrus-admin-acl.h"
#include "gyrus-common.h"

gboolean
gyrus_admin_acl_delete_entry (GyrusAdmin *admin, const gchar *mailbox,
			      const gchar *identifier, gchar **error)
{
	GyrusImapStatus status;

	gchar *msg = g_strdup_printf (". deleteacl \"%s\" \"%s\"\n",
				      mailbox, identifier);
	gyrus_admin_write_channel (admin, msg);
	g_free (msg);

	status = gyrus_admin_listen_channel (admin, &msg, NULL);
	g_free (msg);

	if (error != NULL)
		switch (status) {
		case GYRUS_IMAP_STATUS_NO:
			*error = g_strdup_printf
				(_("Mailbox '%s' does not exist."), mailbox);
			break;
		case GYRUS_IMAP_STATUS_BAD:
		case GYRUS_IMAP_STATUS_BYE:
		case GYRUS_IMAP_STATUS_LIST:
		case GYRUS_IMAP_STATUS_OK:
			break;
		}
	
	return (status == GYRUS_IMAP_STATUS_OK);
}

gboolean
gyrus_admin_acl_set_entry (GyrusAdmin *admin, const gchar *mailbox,
			   const gchar* user, const gchar* permissions,
			   gchar **error)
{
	GyrusImapStatus status;
	gchar *msg;

	if (!gyrus_common_str_is_ascii (user)) {
		if (error != NULL)
			*error = g_strdup(_("Invalid identifier."));
		return FALSE;
	}

	if (strlen (user) == 0) {
		if (error != NULL)
			*error = g_strdup(_("Empty entry name."));
		return FALSE;
	}
	else if (strlen (mailbox) == 0) {
		if (error != NULL)
			*error = g_strdup(_("Empty mailbox name."));
		return FALSE;
	}
	
	msg = g_strdup_printf (". setacl \"%s\" \"%s\" %s\n",
			       mailbox, user, permissions);
	gyrus_admin_write_channel (admin, msg);
	g_free (msg);

	status = gyrus_admin_listen_channel (admin, &msg, NULL);
	g_free (msg);

	if (error != NULL)
		switch (status) {
		case GYRUS_IMAP_STATUS_NO:
			*error = g_strdup_printf
				(_("Mailbox '%s' does not exist."), mailbox);
			break;
		case GYRUS_IMAP_STATUS_BAD:
			*error = g_strdup
				(_("Missing required argument to Setacl"));
		case GYRUS_IMAP_STATUS_BYE:
		case GYRUS_IMAP_STATUS_LIST:
		case GYRUS_IMAP_STATUS_OK:
			break;
		}
	
	return (status == GYRUS_IMAP_STATUS_OK);
}

static gint 
gyrus_admin_acl_sort_func (gconstpointer a,
			   gconstpointer b)
{
	return (strcmp (GYRUS_IMAP_ACL_ENTRY (a)->identifier,
			GYRUS_IMAP_ACL_ENTRY (b)->identifier));
}

GList *
gyrus_admin_acl_get (GyrusAdmin *admin, const gchar *mailbox,
		     gchar **error)
{
	GList *list = NULL;
	GyrusImapStatus status;
	GyrusImapAclEntry *entry;
	gchar *iter, *iter2;
	
	gchar *msg = g_strdup_printf (". getacl \"%s\"\n",
				      mailbox);
	gyrus_admin_write_channel (admin, msg);
	g_free (msg);
	
	status = gyrus_admin_listen_channel (admin, &msg, NULL);

	if (status == GYRUS_IMAP_STATUS_NO) {
		if (strcmp (msg, ". NO Permission denied\n") && error != NULL)
			*error = g_strdup (_("Permission denied"));
		g_free (msg);
		return NULL;
	}

	/* we need to parse the message because of its horrible format.
	   Following code is the most ugly code ever written in earth. It's not
	   difficult to get better, so please if bored, do it.
	 */
	
	/* skips "* ACL " */
	iter = (gchar *)&msg[strlen ("* ACL ")];

	/* skips user.mailbox  or "user.my mailbox" */
	if (*iter == '"') {
		do ++iter; while (*iter != '"');
		iter ++;
		if (*iter == ' ')
			iter ++;

	}
	else {
		do ++iter; while (*iter != ' ' && *iter != '\r');
		if (*iter == ' ')
			iter ++;
	}

	if (*iter == '\r') {
		g_free (msg);
		status = gyrus_admin_listen_channel (admin, &msg, NULL);
		g_free (msg);
		if (error != NULL)
			*error = g_strdup (_("Empty access control list."));
		return NULL;
	}
	
	do {
		entry = g_new (GyrusImapAclEntry, 1);

		iter2 = iter;
		
		/* gets the identifier, if it is sorrounded by spaces,
		   or not. */
		if (*iter2 == '"') {
			iter++; /* skip the '"' */
			do ++iter2; while (*iter2 != '"');
		} else {
			do ++iter2; while (*iter2 != ' ');
		}

		/* we got the identifier, duplicate it */
		entry->identifier = g_strndup (iter, iter2 - iter);

		/* gets the rights */
		do iter2++; while (*iter2 == ' ');
		iter = iter2;
		do ++iter2; while (!isspace (*iter2));
		
		/* we got the rights, duply it */
		entry->rights = g_strndup (iter, iter2 - iter);

		do (iter2++); while (*iter2 == ' ');
		iter = iter2;
		
/*
		  g_print ("identifier: %s.\n", entry->identifier);
		  g_print ("rights: %s.\n", entry->rights);
*/
		
		list = g_list_prepend (list, entry);
	} while (*iter2 != '\0');

	/*
	GList *l_iter;

	for (l_iter = list; l_iter != NULL; l_iter = g_list_next (l_iter))
		g_print ("identifier: %s\nrights: %s\n",
			 GYRUS_IMAP_ACL_ENTRY (l_iter->data)->identifier,
			 GYRUS_IMAP_ACL_ENTRY (l_iter->data)->rights);
	*/
	
	list = g_list_sort (list, gyrus_admin_acl_sort_func);
	g_free (msg);
	status = gyrus_admin_listen_channel (admin, &msg, NULL);
	g_free (msg);

	return list;
}

void
gyrus_admin_acl_list_free (GList *list)
{
	GList *l_iter;
	for (l_iter = list; l_iter != NULL; l_iter = g_list_next (l_iter)) {
		g_free (GYRUS_IMAP_ACL_ENTRY(l_iter->data)->identifier);
		g_free (GYRUS_IMAP_ACL_ENTRY(l_iter->data)->rights);
		g_free (GYRUS_IMAP_ACL_ENTRY(l_iter->data));
	}

	g_list_free (list);
}
