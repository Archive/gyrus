/*
  gyrus-session.h

  GYRUS -- GNOME Cyrus Administrator. Session manager header file.
  
  Copyright (C) 2004 Claudio Saavedra V. <csaavedra@alumnos.utalca.cl>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GYRUS_SESSION_H
#define GYRUS_SESSION_H

#define PATH_KEY "/apps/gyrus/"
#define PATH_SESSIONS "/apps/gyrus/sessions/"

typedef struct _GyrusSession GyrusSession;

struct _GyrusSession
{
	gchar *name;
	gchar *host;
	guint port;
	gchar *user;
	gchar *passwd;
	gchar *sep_char;
	gboolean usetls;
};

/** Callback to load the interface of the sessions system.
*/
void 
gyrus_session_show_sessions (GtkWidget *widget G_GNUC_UNUSED, 
			     gpointer user_data);

/** Frees the memory used for a GyrusSession object. */
void
gyrus_session_free (GyrusSession *session);


#endif /* GYRUS_SESSION_H */
