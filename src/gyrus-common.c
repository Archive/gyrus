/*
  gyrus-common.c - Common modules.

  GYRUS -- GNOME Cyrus Administrator.

  Copyright (C) 2003-2004 Claudio Saavedra V.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, see <http://www.gnu.org/licenses/>.
  
*/

#include <config.h>

#include <gtk/gtk.h>
#include <string.h>
#include "glib/gi18n.h"
#include "gyrus-common.h"

void
gyrus_common_show_message (GtkWindow *parent,
			   GtkMessageType type,
			   const gchar* message)
{
	GtkWidget *dialog;

	dialog = gtk_message_dialog_new (parent, GTK_DIALOG_MODAL,
				 type, GTK_BUTTONS_OK, "%s", message);
	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
}

gboolean
gyrus_gtk_entry_has_text (GtkEntry *entry)
{
	const gchar *text;
        const gchar *start;
        gboolean has_text;

	g_return_val_if_fail (GTK_IS_ENTRY (entry), FALSE);

        text = gtk_entry_get_text (entry);

        start = text;
        while (*start != '\0' && g_ascii_isspace (*start))
                start ++;

        has_text = (start && start[0]);

	return (has_text);
}

gchar *
gyrus_dialog_password_new (void)
{
	gchar *file_xml;
	GtkWidget *dialog;
	GtkWidget *entry_password;
	gchar *password = NULL;
	gint result;
	GtkBuilder *builder;

	file_xml = g_build_filename (GYRUS_UI_DIR, "password.xml", NULL);

        builder = gtk_builder_new ();
        gtk_builder_add_from_file (builder, file_xml, NULL);

	g_free (file_xml);
	
	dialog = GTK_WIDGET (gtk_builder_get_object (builder, "dialog_password"));
	entry_password = GTK_WIDGET (gtk_builder_get_object (builder, "entry_password"));
	
	result = gtk_dialog_run (GTK_DIALOG (dialog));

	switch (result) {
	case GTK_RESPONSE_OK:
		password = g_strdup (gtk_entry_get_text
				     (GTK_ENTRY (entry_password)));
		break;
	case GTK_RESPONSE_CANCEL:
		password = NULL;
	}
	gtk_widget_destroy (dialog);

	g_object_unref (builder);

	return password;
}

gboolean
gyrus_common_str_is_ascii (const gchar *str)
{
	while (*str) {
		if ((guchar)*str >= 128)
			return FALSE;
		str++;
	}
	return TRUE;
}
