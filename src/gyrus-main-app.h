/*
  gyrus-main-app.h

  GYRUS -- GNOME Cyrus Administrator Main Application Class header definitions.
  
  Copyright (C) 2004 Claudio Saavedra Vald�s <csaavedra@alumnos.utalca.cl>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#ifndef GYRUS_MAIN_APP_H
#define GYRUS_MAIN_APP_H

/* #include <libgnomeui/libgnomeui.h> */

#include <glib-object.h>

#define GYRUS_TYPE_MAIN_APP            (gyrus_main_app_get_type ())
#define GYRUS_MAIN_APP(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GYRUS_TYPE_MAIN_APP, GyrusMainApp))
#define GYRUS_MAIN_APP_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass),  GYRUS_TYPE_MAIN_APP, GyrusMainAppClass))
#define GYRUS_IS_MAIN_APP(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GYRUS_TYPE_MAIN_APP))
#define GYRUS_IS_MAIN_APP_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GYRUS_TYPE_MAIN_APP))
#define GYRUS_MAIN_APP_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj),  GYRUS_TYPE_MAIN_APP, GyrusMainApp))

typedef struct _GyrusMainAppPrivate GyrusMainAppPrivate;
typedef struct _GyrusMainApp        GyrusMainApp;
typedef struct _GyrusMainAppClass   GyrusMainAppClass;

/*** Gyrus Main Application Data ***/
struct _GyrusMainApp {
	GtkWindow window;
	GyrusMainAppPrivate *priv;
};

/*** Gyrus Main Application Class Data ***/
/*** derived from GtkWindowClass ***/
struct _GyrusMainAppClass {
	GtkWindowClass window_class;
};

#include "gyrus-session.h"
#include "gyrus-admin.h"

GType gyrus_main_app_get_type (void);

/** Creates a new instance of the main application. 

Normally, you would want to run this just once.

Returns: A new main application window.

*/
GtkWidget * gyrus_main_app_new (void);

/** 
    Loads a session in the main application.

    Loads the given @session in the @app, by opening a new tab and selecting it.
    This will create a new instance of #GyrusAdmin so it can be manipulated.
    
    @app: A #GyrusMainApp instance.
    @session: The #GyrusSession to load.

    Returns: The new #GyrusAdmin instance that is loaded in.
*/
GtkWidget * gyrus_main_app_append_page (GyrusMainApp *app,
					GyrusSession *session);

/** 
    Changes the sensitivity of items in the menu. The 
    menu items that get affected to the sensitivity change
    are those related to the connected/desconnected 
    status of the GyrusAdmin. Use it when changing the page
    and when (des)connecting a GyrusAdmin.

    @app: A #GyrusMainApp instance.
    @sens: TRUE to set the sensitivity FALSE to unset it.

*/
void gyrus_main_app_menu_set_sensitive (GyrusMainApp *app, gboolean sens);

/**
   Gets the currently selected #GyrusAdmin, i.e. the #GyrusAdmin 
   that is visible in the application.

   @app: A #GyrusMainApp instance.
 
   Returns: The currently selected #GyrusAdmin.
 */
GyrusAdmin *gyrus_main_app_get_current_admin (GyrusMainApp *app);

/**
   Toggles the sensitivity of a item in the menu of @app.
   You need to pass the path to the item according to the schema.
   
   @app: A #GyrusMainApp instance.
   @command: The path to the item.
   @sensitive:
*/
void gyrus_main_app_menu_item_set_sensitive (GyrusMainApp *app,
					     const gchar *command,
					     gboolean sensitive);
#endif /* GYRUS_MAIN_APP_H */
