# Hungarian messages for gyrus.
# Copyright (C) 2006 Free Software Foundation
# This file is distributed under the same license as the gyrus package.
#
# Nora Albitz <albitz.nora at gmail dot com>, 2006.
# Gabor Kelemen <kelemeng at gnome dot hu>, 2006, 2010.
# Balázs Úr <urbalazs at gmail dot com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: gyrus master\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=gyrus&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2012-11-09 17:03+0000\n"
"PO-Revision-Date: 2013-03-17 11:13+0100\n"
"Last-Translator: Balázs Úr <urbalazs at gmail dot com>\n"
"Language-Team: Hungarian <gnome at fsf dot hu>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms:  nplurals=2; plural=(n != 1);\n"

#: ../gyrus.desktop.in.in.h:1
msgid "Gyrus IMAP Cyrus Administrator"
msgstr "Gyrus IMAP Cyrus adminisztrátor"

#: ../gyrus.desktop.in.in.h:2
msgid "Administer the mailboxes of your IMAP Cyrus servers"
msgstr "Az IMAP Cyrus kiszolgálók levélmappáinak adminisztrációja"

#: ../src/ui/create_mailbox.xml.h:1
msgid "Name:"
msgstr "Név:"

#: ../src/ui/create_mailbox.xml.h:2
#| msgid "Quota (%)"
msgid "Quota (MB):"
msgstr "Kvóta (MB):"

#: ../src/ui/create_mailbox.xml.h:3
msgid "Assign quota"
msgstr "Kvóta meghatározása"

#: ../src/ui/find.xml.h:1
msgid "Search for:"
msgstr "Keresés erre:"

#: ../src/ui/find.xml.h:2
msgid "Match _entire word only"
msgstr "_Csak teljes szóra"

#: ../src/ui/find.xml.h:3
msgid "_Wrap around"
msgstr "_Körbe"

#: ../src/ui/page.xml.h:1
#| msgid "<b>Free space:</b>"
msgid "Free space:"
msgstr "Szabad hely:"

#: ../src/ui/page.xml.h:2
#| msgid "<b>Assigned space:</b>"
msgid "Assigned space:"
msgstr "Kiosztott hely:"

#: ../src/ui/page.xml.h:3
#| msgid "<b>Owner:</b>"
msgid "Owner:"
msgstr "Tulajdonos:"

#: ../src/ui/page.xml.h:4
msgid "New quota (MB)"
msgstr "Új kvóta (MB)"

#: ../src/ui/page.xml.h:5
msgid "Modify quota"
msgstr "Kvóta módosítása"

#: ../src/ui/page.xml.h:6
msgid "Access control list"
msgstr "Hozzáférés-felügyeleti lista"

#: ../src/ui/page.xml.h:7 ../src/ui/sessions_edit.xml.h:5
#: ../tests/gyrus-talk.xml.h:2
msgid "Host:"
msgstr "Kiszolgáló:"

#: ../src/ui/page.xml.h:8
#| msgid "User"
msgid "User:"
msgstr "Felhasználó:"

#: ../src/ui/page.xml.h:9 ../src/ui/sessions_edit.xml.h:4
#: ../tests/gyrus-talk.xml.h:4
msgid "Port:"
msgstr "Port:"

#: ../src/ui/password.xml.h:1
msgid "Password"
msgstr "Jelszó"

#: ../src/ui/password.xml.h:2
#| msgid "<b>Enter your password</b>"
msgid "Enter your password"
msgstr "Adja meg a jelszavát"

#: ../src/ui/report.xml.h:1
msgid "Report"
msgstr "Jelentés"

#: ../src/ui/report.xml.h:3
#, no-c-format
msgid "Over (%)"
msgstr "Felett (%)"

#: ../src/ui/sessions.xml.h:1
msgid "Open session"
msgstr "Munkafolyamat megnyitása"

#: ../src/ui/sessions_edit.xml.h:1
msgid "Session name:"
msgstr "Munkamenet neve:"

#: ../src/ui/sessions_edit.xml.h:2
msgid "Password:"
msgstr "Jelszó:"

#: ../src/ui/sessions_edit.xml.h:3
msgid "Username:"
msgstr "Felhasználónév:"

#: ../src/ui/sessions_edit.xml.h:6
#| msgid "<b>Session details</b>"
msgid "Session details"
msgstr "Munkafolyamat részletei"

#: ../src/ui/sessions_edit.xml.h:7 ../tests/gyrus-talk.xml.h:3
msgid "Use a secure connection"
msgstr "Biztonságos kapcsolat használata"

#: ../src/ui/sessions_edit.xml.h:8
msgid "Mailbox hierarchy separator:"
msgstr "Levélmappa hierarchia-elválasztó"

#: ../src/ui/sessions_edit.xml.h:9
msgid "<b>Options</b>"
msgstr "<b>Beállítások</b>"

#: ../src/gyrus-admin-acl.c:54 ../src/gyrus-admin-acl.c:103
#, c-format
msgid "Mailbox '%s' does not exist."
msgstr "A levélmappa (%s) nem létezik."

#: ../src/gyrus-admin-acl.c:76
msgid "Invalid identifier."
msgstr "Érvénytelen azonosító."

#: ../src/gyrus-admin-acl.c:82
msgid "Empty entry name."
msgstr "Üres bejegyzésnév."

#: ../src/gyrus-admin-acl.c:87
msgid "Empty mailbox name."
msgstr "Üres postafióknév."

#: ../src/gyrus-admin-acl.c:107
msgid "Missing required argument to Setacl"
msgstr "Hiányzó kötelező paraméter a Setacl-hez"

#: ../src/gyrus-admin-acl.c:143 ../src/gyrus-admin-mailbox.c:80
msgid "Permission denied"
msgstr "Hozzáférés megtagadva"

#: ../src/gyrus-admin-acl.c:175
msgid "Empty access control list."
msgstr "Üres hozzáférés-felügyeleti lista."

#: ../src/gyrus-admin-mailbox.c:78
msgid "Quota does not exist"
msgstr "A kvóta nem létezik"

#: ../src/gyrus-admin-mailbox.c:172
#, c-format
msgid "Quota overloaded"
msgstr "Kvóta túllépve"

#: ../src/gyrus-admin-mailbox.c:230
msgid "Quota not valid. Please try again."
msgstr "A kvóta nem érvényes. Próbálja meg újra."

#: ../src/gyrus-admin-mailbox.c:244
msgid ""
"Unable to change quota. Are you sure do you have the appropriate permissions?"
msgstr ""
"A kvótaváltoztatás meghiúsult. Biztosan rendelkezik a megfelelő "
"jogosultsággal?"

#: ../src/gyrus-admin-mailbox.c:324
#, c-format
msgid "'%s' is not a valid mailbox name. Please try a different one."
msgstr "A(z) „%s” nem érvényes postafióknév. Próbáljon egy másikat."

#: ../src/gyrus-admin-mailbox.c:332
#, c-format
msgid ""
"Parent mailbox '%s' does not exist. Please refresh the mailboxes list and "
"try again."
msgstr ""
"A szülőlevélmappa (%s) nem létezik. Frissítse a levélmappalistát és próbálja "
"újra."

#: ../src/gyrus-admin-mailbox.c:342
#, c-format
msgid "Mailbox '%s' already exists. Please try a different name."
msgstr "Már létezik postafiók „%s” néven. Adjon másik nevet."

#: ../src/gyrus-admin-mailbox.c:355
msgid ""
"Unable to create the mailbox. Are you sure do you have the appropriate "
"permissions?"
msgstr ""
"Nem lehet a postafiókot létrehozni. Biztosan rendelkezik a megfelelő "
"jogosultsággal?"

#: ../src/gyrus-admin-mailbox.c:366
msgid "Mailbox created, but could not set quota."
msgstr "Postafiók létrehozva, de a kvóta nem lett beállítva."

#: ../src/gyrus-admin-mailbox.c:433
#, c-format
msgid "Unable to delete '%s'. Permission denied."
msgstr "A(z) „%s” nem törölhető. Engedély megtagadva."

#: ../src/gyrus-admin-mailbox.c:634
msgid "new entry"
msgstr "új bejegyzés"

#: ../src/gyrus-admin.c:447 ../src/gyrus-report.c:289
#, c-format
msgid "Users (%d)"
msgstr "Felhasználók (%d)"

#: ../src/gyrus-admin.c:453
#, c-format
msgid "Orphaned mailboxes (%d)"
msgstr "Árva postafiókok (%d)"

#: ../src/gyrus-admin.c:497 ../src/gyrus-admin.c:855
msgid "Orphaned mailboxes"
msgstr "Árva postafiókok"

#: ../src/gyrus-admin.c:497 ../src/gyrus-admin.c:853 ../src/gyrus-report.c:170
msgid "Users"
msgstr "Felhasználók"

#: ../src/gyrus-admin.c:565
msgid "lookup"
msgstr "keresés"

#: ../src/gyrus-admin.c:566
msgid "read"
msgstr "olvasás"

#: ../src/gyrus-admin.c:567
msgid "seen"
msgstr "olvasott"

#: ../src/gyrus-admin.c:568
msgid "write"
msgstr "írás"

#: ../src/gyrus-admin.c:569
msgid "insert"
msgstr "beszúrás"

#: ../src/gyrus-admin.c:570
msgid "post"
msgstr "küldés"

#: ../src/gyrus-admin.c:571
msgid "create"
msgstr "létrehozás"

#: ../src/gyrus-admin.c:572
msgid "delete"
msgstr "törlés"

#: ../src/gyrus-admin.c:573
msgid "admin"
msgstr "admin"

#: ../src/gyrus-admin.c:581
msgid "Identifier"
msgstr "Azonosító"

#: ../src/gyrus-admin.c:651
msgid "Couldn't create the client socket."
msgstr "Nem hozható létre a kliens foglalat."

#: ../src/gyrus-admin.c:659
msgid "Couldn't parse the server address."
msgstr "Nem dolgozható fel a kiszolgáló cím."

#: ../src/gyrus-admin.c:668
#, c-format
msgid "Could not connect to %s, port %d."
msgstr "Nem sikerült a csatlakozás, kiszolgáló: %s, port: %d."

#: ../src/gyrus-admin.c:1001
msgid "Unable to connect with empty passwords. Please introduce your password."
msgstr "Jelszó nélkül nem lehet csatlakozni. Adja meg a jelszavát."

#: ../src/gyrus-admin.c:1008
msgid "Incorrect login/password"
msgstr "Helytelen felhasználónév/jelszó"

#: ../src/gyrus-admin.c:1364
msgid "Could not change permission. Server error: "
msgstr "A jogosultság nem változtatható. Kiszolgálóhiba: "

#: ../src/gyrus-admin.c:1407
#, c-format
msgid "An entry called '%s' already exists. Overwrite it?"
msgstr "A bejegyzés (%s) már létezik. Felülírja?"

#: ../src/gyrus-dialog-find-mailbox.c:171
#, c-format
msgid "The text '%s' was not found in the mailbox list."
msgstr "A szöveg (%s) nem található a levélmappa listában."

#: ../src/gyrus-dialog-find-mailbox.c:275
msgid "Find mailbox"
msgstr "Levélmappa keresése"

#: ../src/gyrus-dialog-mailbox-new.c:93
msgid "Quota not valid"
msgstr "A kvóta nem érvényes"

#: ../src/gyrus-dialog-mailbox-new.c:220
msgid "New mailbox"
msgstr "Új levélmappa"

#: ../src/gyrus-main-app.c:146
#, c-format
msgid "Really delete mailbox '%s' and all of its submailboxes?"
msgstr "Biztosan törli a(z) „%s” levélmappát az összes almappájával?"

#: ../src/gyrus-main-app.c:261 ../src/gyrus-main-app.c:436
#: ../src/gyrus-main-app.c:732
msgid "Cyrus IMAP Administrator"
msgstr "Cyrus IMAP adminisztrátor"

#: ../src/gyrus-main-app.c:275
#, c-format
msgid "%s - Cyrus IMAP Administrator"
msgstr "%s - Cyrus IMAP adminisztrátor"

#: ../src/gyrus-main-app.c:383
msgid "_File"
msgstr "_Fájl"

#: ../src/gyrus-main-app.c:384
msgid "_Edit"
msgstr "S_zerkesztés"

#: ../src/gyrus-main-app.c:385
msgid "_ACL"
msgstr "_ACL"

#: ../src/gyrus-main-app.c:386
msgid "_View"
msgstr "_Nézet"

#: ../src/gyrus-main-app.c:387
msgid "_Help"
msgstr "_Súgó"

#: ../src/gyrus-main-app.c:388
msgid "Go to server..."
msgstr "Csatlakozás a kiszolgálóhoz..."

#: ../src/gyrus-main-app.c:389
msgid "Show the list of servers"
msgstr "Kiszolgálólista megjelenítése"

#: ../src/gyrus-main-app.c:397
msgid "Add mailbox"
msgstr "Levélmappa hozzáadása"

#: ../src/gyrus-main-app.c:398
msgid "Add a mailbox under the one selected"
msgstr "Levélmappa hozzáadása a kijelölt alá"

#: ../src/gyrus-main-app.c:400
msgid "Search for a mailbox in current server"
msgstr "Levélmappa keresése a jelenlegi kiszolgálón"

#: ../src/gyrus-main-app.c:402
msgid "Refresh the mailbox list"
msgstr "A levélmappalista frissítése"

#: ../src/gyrus-main-app.c:403
msgid "Create report..."
msgstr "Jelentés létrehozása…"

#: ../src/gyrus-main-app.c:404
msgid "Create report of users with quota problems"
msgstr "Jelentés létrehozása a kvótaproblémás felhasználókról."

#: ../src/gyrus-main-app.c:408
msgid "New entry"
msgstr "Új bejegyzés"

#: ../src/gyrus-main-app.c:409
msgid "Create a new ACL entry in current mailbox"
msgstr "Egy új ACL bejegyzés létrehozása a jelenlegi levélmappában"

#: ../src/gyrus-main-app.c:410
msgid "Remove mailbox"
msgstr "Levélmappa eltávolítása"

#: ../src/gyrus-main-app.c:411
msgid "Remove current mailbox from the server"
msgstr "Jelenlegi levélmappa eltávolítása a kiszolgálóról"

#: ../src/gyrus-main-app.c:416
msgid "Rename entry"
msgstr "Bejegyzés átnevezése"

#: ../src/gyrus-main-app.c:417
msgid "Rename selected ACL entry"
msgstr "Kiválasztott ACL bejegyzés átnevezése"

#: ../src/gyrus-main-app.c:418
msgid "Delete entry"
msgstr "Bejegyzés törlése"

#: ../src/gyrus-main-app.c:419
msgid "Delete selected ACL entry"
msgstr "Kiválasztott ACL bejegyzés törlése"

#: ../src/gyrus-main-app.c:552
msgid "translators-credits"
msgstr ""
"Albitz Nóra <albitz.nora@gmail.com>\n"
"Kelemen Gábor <kelemeng@gnome.hu>"

#: ../src/gyrus-main-app.c:562
msgid "GNOME Cyrus Administrator"
msgstr "GNOME Cyrus adminisztrátor"

#: ../src/gyrus-main-app.c:564
msgid ""
"(c) 2003-2005 GNOME Foundation\n"
"(c) 2004-2005 Claudio Saavedra"
msgstr ""
"(c) 2003-2005 GNOME Alapítvány\n"
"(c) 2004-2005 Claudio Saavedra"

#: ../src/gyrus-main-app.c:566
msgid "Administration tool for Cyrus IMAP servers."
msgstr "Adminisztrációs eszköz Cyrus IMAP kiszolgálókhoz."

#. set title
#: ../src/gyrus-report.c:107
#, c-format
msgid "Mailbox space usage report for %s"
msgstr "Postafiók-használati jelentés a következőhöz: %s"

#: ../src/gyrus-report.c:191 ../src/gyrus-report.c:492
msgid "Quota (%)"
msgstr "Kvóta (%)"

#: ../src/gyrus-report.c:204 ../src/gyrus-report.c:496
msgid "Assigned (KB)"
msgstr "Kijelölt (KB)"

#: ../src/gyrus-report.c:215 ../src/gyrus-report.c:500
msgid "Used (KB)"
msgstr "Felhasznált (KB)"

#. Translators: this represents the number of pages being printed.
#: ../src/gyrus-report.c:465
#, c-format
msgid "%d/%d"
msgstr "%d/%d"

#: ../src/gyrus-report.c:488
msgid "User"
msgstr "Felhasználó"

#: ../src/gyrus-session.c:164
msgid "Edit session"
msgstr "Munkafolyamat szerkesztése"

#: ../src/gyrus-session.c:178
msgid "New session"
msgstr "Új munkafolyamat"

#: ../src/gyrus-session.c:357
msgid "A session name is required."
msgstr "Adjon nevet a munkafolyamatnak."

#: ../src/gyrus-session.c:368
#, c-format
msgid "Session named \"%s\" already exists."
msgstr "Már létezik munkafolyamat „%s” néven."

#: ../src/gyrus-session.c:464 ../src/gyrus-session.c:598
msgid "Autodetect"
msgstr "Automatikus felismerés"

#: ../src/gyrus-session.c:549
msgid "Session"
msgstr "Munkafolyamat"

#: ../src/gyrus-session.c:718
msgid "No host specified."
msgstr "Nincs megadva kiszolgáló."

#: ../tests/gyrus-talk.xml.h:1
msgid "Talk - Echo client"
msgstr "Talk - Echo kliens"

#: ../tests/gyrus-talk.xml.h:5
msgid "_Connect"
msgstr "_Csatlakozás"

#: ../tests/gyrus-talk.xml.h:6
#| msgid "_Connect"
msgid "Connection"
msgstr "Kapcsolat"

#: ../tests/gyrus-talk.xml.h:7
msgid "Command:"
msgstr "Parancs:"

#: ../tests/gyrus-talk.xml.h:8
msgid "_Send"
msgstr "_Küldés"

#~ msgid "*"
#~ msgstr "*"

#~ msgid "<b>Name:</b>"
#~ msgstr "<b>Név:</b>"

#~ msgid "<b>Quota (MB):</b>"
#~ msgstr "<b>Kvóta (MB):</b>"

#~ msgid "Create mailbox"
#~ msgstr "Levélmappa létrehozása"

#~ msgid "Find"
#~ msgstr "Keresés"

#~ msgid "<b>Host:</b>"
#~ msgstr "<b>Kiszolgáló:</b>"

#~ msgid "<b>Port:</b>"
#~ msgstr "<b>Port:</b>"

#~ msgid "<b>User:</b>"
#~ msgstr "<b>Felhasználó:</b>"

#~ msgid "<b>Mailbox quota:</b>"
#~ msgstr "<b>Levélmappa kvóta:</b>"

#~ msgid "<b>Mailboxes tree:</b>"
#~ msgstr "<b>Levélmappa fa:</b>"

#~ msgid "Default suffix for changing quota:"
#~ msgstr "Alapértelmezett utótag a kvóta megváltoztatásához:"

#~ msgid "Preferences"
#~ msgstr "Beállítások"

#~ msgid "View complete mailboxes tree"
#~ msgstr "Teljes postafiók fanézetben"

#~ msgid ""
#~ ".\n"
#~ "/\n"
#~ "Autodetect"
#~ msgstr ""
#~ ".\n"
#~ "/\n"
#~ "Automatikus felismerés"

#~ msgid "%s could not be found. Please check the name and try again."
#~ msgstr "%s nem található. Ellenőrizze a nevet és próbálja meg újra."

#~ msgid "_Disconnect"
#~ msgstr "_Kilépés"

#~ msgid "<b>Connection</b>"
#~ msgstr "<b>Kapcsolat</b>"
