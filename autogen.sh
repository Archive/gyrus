#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="gyrus"

(test -f $srcdir/configure.ac) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level directory"
    exit 1
}

which gnome-autogen.sh || {
    echo "You need to install gnome-common to compile $PKG_NAME. "
    echo "Install the appropiate package for your distribution, "
    echo "or get the tarball from "
    echo "http://ftp.gnome.org/pub/GNOME/sources/gnome-common/"
    exit 1
}
REQUIRED_INTLTOOL_VERSION=0.40.0
REQUIRED_AUTOMAKE_VERSION=1.9 USE_GNOME2_MACROS=1 . gnome-autogen.sh
